<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 |--------------------------------------------------------------------------
 | Base Site URL
 |--------------------------------------------------------------------------
 |
 | URL to your CodeIgniter root. Typically this will be your base URL,
 | WITH a trailing slash:
 |
 |    http://example.com/
 |
 | If this is not set then CodeIgniter will guess the protocol, domain and
 | path to your installation.
 |
 */
$config['base_url']    = 'http://localhost/codeigniter/';  //***NOTA***: Se debe mantener igual tambien en: $config['project_root'] luego de la IP o host, el mismo valor

/*
 |--------------------------------------------------------------------------
 | Ruta raiz de los recursos web
 |--------------------------------------------------------------------------
 | Esta variable se ha creado para definir la raiz donde ubicaremos los recursos
 | web que se descargan al navegar (imgs, css, js, etc).
 | NOTA: Esta variable debe setearse siempre luego de $config['base_url']
 */
$config['uri_sources'] = 'application/sources/';//
$config['sources'] = $config['base_url'].'application/sources/';//

/*
 |--------------------------------------------------------------------------
 | Ruta raiz de los recursos web para páginas FAQ
 |--------------------------------------------------------------------------
 | Esta variable se ha creado para definir la raiz donde ubicaremos los recursos
 | web que se descargan al navegar (imgs, css, js, etc).
 | NOTA: Esta variable debe setearse siempre luego de $config['base_url']
 */
$config['faq_src'] = $config['base_url'].'application/views/admin_faq/';//

/*
 |--------------------------------------------------------------------------
 | Ruta raiz del proyecto
 |--------------------------------------------------------------------------
 | Esta variable se ha creado para definir la raiz del proyecto. Se usa la
 | variable $_SERVER['DOCUMENT_ROOT'] y el resto de la cadena indica el
 | sub-directorio del proyecto dentro del servidor (lo que sería su ruta
 | relativa), hasta la carpeta raíz del framework.
 */
$config['project_root'] = $_SERVER["DOCUMENT_ROOT"].'/codeigniter/'; // SI USA VIRTUAL HOST NO DEBERÍA APLICAR NADA AQUÍ...

/*
 |--------------------------------------------------------------------------
 | mostrar el 'profiler' codeigniter debug
 |--------------------------------------------------------------------------
 | Para mostrar el profiler de codeigniter
 */
$config['enable_profiler'] = TRUE;

/*
 |--------------------------------------------------------------------------
 | Variable para modo mantenimiento
 |--------------------------------------------------------------------------
 | Con esta variable forzaremos que el sitio web, a petición de esta variable de configuración
 | sea puesto en modo de mantenimiento (pagina que indica que está en mantenimiento), impidiendo
 | cualquier otra ejecución/acción sobre la aplicación.
 */
$config['maintenance_mode'] = FALSE;

/*
 |--------------------------------------------------------------------------
 | Variable para permitir
 |--------------------------------------------------------------------------
 | Se emplea para registrar las direcciones IP que tendrán acceso a la aplicación,
 | cuando esta se encuentre en modo de mantenimiento.
 */
$config['maintenance_allow_ip'] = array('localhost','127.0.0.1');

/*
 |--------------------------------------------------------------------------
 | Session Variables
 |--------------------------------------------------------------------------
 |
 | ...
 |
 */
$config['sess_cookie_name'] = 'sme_ci_session';
$config['sess_save_path'] = 'ci_sessions';