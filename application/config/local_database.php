<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// SUSTITUIR VALORES DENTRO DE '--...--' => POR EJEMPLO '--HOST--' => 'localhost'
$db['default'] = array(
		'dsn'	=> '',
		'hostname' => 'localhost',
		'username' => 'sistemalaravel',
		'password' => 'sistema123',
		'database' => 'corpbid',
		'schema' => '',
		'dbdriver' => 'mysqli',
		'dbprefix' => '',
		'pconnect' => FALSE,
		'db_debug' => (ENVIRONMENT !== 'production'),
		'cache_on' => FALSE,
		'cachedir' => '',
		'char_set' => 'utf8',
		'dbcollat' => 'utf8_general_ci',
		'swap_pre' => '',
		'encrypt' => FALSE,
		'compress' => FALSE,
		'stricton' => FALSE,
		'failover' => array(),
		'save_queries' => TRUE
);

